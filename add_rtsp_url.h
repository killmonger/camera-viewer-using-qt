#ifndef ADD_RTSP_URL_H
#define ADD_RTSP_URL_H
#include <QWidget>
#include <QDialog>
#include <QCamera>
#include <QCameraInfo>
#include "ui_add_rtsp_url.h"

namespace Ui {
class add_rtsp_url;
}

class add_rtsp_url : public QDialog
{
    Q_OBJECT

public:
    explicit add_rtsp_url(QWidget *parent = 0);
    ~add_rtsp_url();
signals:
    void add_a_device(QString& device, QString& rtsp);

public slots:
    void on_cancel();
    void on_ok();

private:
    Ui::add_rtsp_url *ui;
};
#endif // ADD_RTSP_URL_H
