#ifndef ADD_DEVICE_H
#define ADD_DEVICE_H
#include <QWidget>
#include <QDialog>
#include <QCamera>
#include <QCameraInfo>
#include "ui_add_device_url.h"


namespace Ui {
class add_device_url;
}

class add_device_url : public QDialog
{
    Q_OBJECT

public:
    explicit add_device_url(QWidget *parent = 0);
    ~add_device_url();
signals:
    void add_a_device(QString& device, QString& rtsp);

public slots:
    void on_cancel();
    void on_ok();

private:
    Ui::add_device_url *ui;
};


#endif // ADD_DEVICE_H
