


#include "add_device_url.h"
#include "ui_add_device_url.h"

add_device_url::add_device_url(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_device_url)
{
    ui->setupUi(this);
    connect(ui->ok, SIGNAL(clicked(bool)), this, SLOT(on_ok()));
    connect(ui->cancel, SIGNAL(clicked(bool)), this, SLOT(on_cancel()));
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    foreach (const QCameraInfo &cameraInfo, cameras) {
        ui->comboBox->addItem(cameraInfo.deviceName());
        }

}

add_device_url::~add_device_url()
{
    delete ui;
}

void add_device_url::on_cancel()
{
    this->close();
}

void add_device_url::on_ok()
{
    QString device = ui->comboBox->currentText();
    QString rtsp = NULL;
    emit add_a_device(device, rtsp);
    this->close();
}
