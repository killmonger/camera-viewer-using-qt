



#include "add_rtsp_url.h"
#include "ui_add_rtsp_url.h"


add_rtsp_url::add_rtsp_url(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_rtsp_url)
{
    ui->setupUi(this);
    connect(ui->ok ,SIGNAL(clicked()), this, SLOT(on_ok()));
    connect(ui->cancel,SIGNAL(clicked()), this, SLOT(on_cancel()));


}

add_rtsp_url::~add_rtsp_url()
{
    delete ui;
}

void add_rtsp_url::on_cancel()
{
    this->close();
}

void add_rtsp_url::on_ok()
{
    QString device = NULL;
    QString rtsp = ui->rtsp->text();
    emit add_a_device(device, rtsp);
    this->close();
}



