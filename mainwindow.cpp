#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    url = new add_rtsp_url(this);
    durl = new add_device_url(this);

    QMenu *file = menuBar()->addMenu("FILE");
    QAction *rtsp_url = file->addAction("RTSP");
    QAction *device_url = file->addAction("DEVICE");
    QAction *play = file->addAction("play");

    QAction *stop = file->addAction("stop");
    QAction *exit = file->addAction("exit");
    connect(play, SIGNAL(triggered(bool)), this, SLOT(play()));

    connect(stop, SIGNAL(triggered()), this, SLOT(stop()));
    connect(rtsp_url, SIGNAL(triggered()), this, SLOT(open_url()));
    connect(device_url, SIGNAL(triggered(bool)), this, SLOT(open_durl()));
    connect(exit, SIGNAL(triggered()), this, SLOT(Exit()));
    connect(url, SIGNAL(add_a_device(QString&,QString&)), this, SLOT(recvDevice(QString&,QString&)));
    connect(durl, SIGNAL(add_a_device(QString&,QString&)), this, SLOT(recvDevice(QString&,QString&)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open_url()
{
    url->show();
}

void MainWindow::open_durl()
{
    durl->show();
}

void MainWindow::Exit()
{
    this->close();
}

void MainWindow::recvDevice(QString& device, QString& rtsp)
{
    if (videoWidget < 4) {
        if (device != NULL) {
           add_device(device);
        } else if (rtsp != NULL) {
            add_link(rtsp);
        }
        videoWidget++;
    }
}

void MainWindow::add_device(QString& device)
{
    if (videoWidget == 0) {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            if (cameraInfo.deviceName() == device) {
            camera_1 = new QCamera(cameraInfo);
            break;
            }
        }
        camera_1->setViewfinder(ui->vw_1);

        camera_1->setCaptureMode(QCamera::CaptureVideo);
        //camera_1->start();
    }
    if (videoWidget == 1) {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            if (cameraInfo.deviceName() == device) {
            camera_2 = new QCamera(cameraInfo);
            break;
            }
        }
        camera_2->setViewfinder(ui->vw_2);
        //camera_2->start();
    }
    if (videoWidget == 2) {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            if (cameraInfo.deviceName() == device) {
            camera_3 = new QCamera(cameraInfo);
            break;
            }
        }
        camera_3->setViewfinder(ui->vw_3);
        //camera_3->start();
    }
    if (videoWidget == 3) {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            if (cameraInfo.deviceName() == device) {
            camera_4 = new QCamera(cameraInfo);
            break;
            }
        }
        camera_4->setViewfinder(ui->vw_4);
        //camera_4->start();
    }

}

void MainWindow::add_link(QString& rtsp)
{
    if (videoWidget == 0) {
        player1 = new QMediaPlayer(this);
        player1->setVideoOutput(ui->vw_1);
        QNetworkRequest request_url_1(rtsp);
        player1->setMedia(request_url_1);
        //player1->play();
        player1->setMuted(true);
    }
    if (videoWidget == 1) {
        player2 = new QMediaPlayer(this);
        player2->setVideoOutput(ui->vw_2);
        QNetworkRequest request_url_2(rtsp);
        player2->setMedia(request_url_2);
        //player2->play();
        player2->setMuted(true);
    }
    if (videoWidget == 2) {
        player3 = new QMediaPlayer(this);
        player3->setVideoOutput(ui->vw_3);
        QNetworkRequest request_url_3(rtsp);
        player3->setMedia(request_url_3);
        //player3->play();
        player3->setMuted(true);
    }
    if (videoWidget == 3) {
        player4 = new QMediaPlayer(this);
        player4->setVideoOutput(ui->vw_4);
        QNetworkRequest request_url_4(rtsp);
        player4->setMedia(request_url_4);
        //player4->play();
        player4->setMuted(true);
    }

}

void MainWindow::play()
{
    if (player1 != NULL)
        player1->play();
    if (player2 != NULL)
        player2->play();
    if (player3 != NULL)
        player3->play();
    if (player4 != NULL)
        player4->play();
    if (camera_1 != NULL)
        camera_1->start();
    if (camera_2 != NULL)
        camera_2->start();
    if (camera_3 != NULL)
        camera_3->start();
    if (camera_4 != NULL)
        camera_4->start();

}



void MainWindow::recordError(QMediaRecorder::Error error)
{
    qDebug() << "record error:" << error;
}

void MainWindow::stop()
{
    if (player1 != NULL)
        player1->stop();
    if (player2 != NULL)
        player2->stop();
    if (player3 != NULL)
        player3->stop();
    if (player4 != NULL)
        player4->stop();
    if (camera_1 != NULL)
        camera_1->stop();
    if (camera_2 != NULL)
        camera_2->stop();
    if (camera_3 != NULL)
        camera_3->stop();
    if (camera_4 != NULL)
        camera_4->stop();

}
