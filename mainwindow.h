#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QGridLayout>
#include <QMediaPlayer>
#include <QNetworkRequest>
#include <QVideoWidget>
#include <QCamera>
#include <QCameraInfo>
#include <QVideoEncoderSettings>
#include <QFile>
#include <QMediaRecorder>
#include <QDateTime>
#include "add_rtsp_url.h"
#include "add_device_url.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void add_device(QString& device);
    void add_link(QString& rtsp);

public slots:
    void open_url();
    void open_durl();
    void Exit();
    void play();

    void stop();
    void recordError(QMediaRecorder::Error error);
    void recvDevice(QString& device, QString& rtsp);

private:
    Ui::MainWindow *ui;
    add_rtsp_url *url;
    add_device_url *durl;
    QMediaPlayer *player1 = NULL;
    QMediaPlayer *player2 = NULL;
    QMediaPlayer *player3 = NULL;
    QMediaPlayer *player4 = NULL;
    QCamera *camera_1 = NULL;
    QCamera *camera_2 = NULL;
    QCamera *camera_3 = NULL;
    QCamera *camera_4 = NULL;

    int videoWidget = 0;
};

#endif // MAINWINDOW_H
